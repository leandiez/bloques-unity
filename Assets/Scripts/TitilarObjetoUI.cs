using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitilarObjetoUI : MonoBehaviour
{
    Text textObject;
    [SerializeField] float blinkFactor = 1.0f;
    void Start()
    {
        textObject = GetComponent<Text>();
    }

    void Update()
    {
        Color currentAlpha = textObject.color;
        if (currentAlpha.a <= 1.0f && currentAlpha.a >= 0.0f)
        {
            currentAlpha.a -= Time.deltaTime * blinkFactor;
            textObject.color = currentAlpha;
        }
        else
        {
            currentAlpha.a = 1.0f;
            textObject.color = currentAlpha;
        }
    }
}
