﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BotonSalir : MonoBehaviour
{
    public bool salir_juego;
    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) && salir_juego){
            Application.Quit();
        }else if(Input.GetKeyDown(KeyCode.Escape) && !salir_juego){
            SceneManager.LoadScene("Portada");
        }
    }
}
