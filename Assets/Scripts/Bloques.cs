﻿using UnityEngine;
using System.Collections;
public class Bloques : MonoBehaviour {

	public GameObject particulasBloque;
	public GestorPuntos adminPuntos;
	/// <summary>
	/// OnCollisionEnter is called when this collider/rigidbody has begun
	/// touching another rigidbody/collider.
	/// </summary>
	/// <param name="other">The Collision data associated with this collision.</param>
	void OnCollisionEnter(Collision other){
		Destroy(gameObject);
		transform.SetParent(null);
		Instantiate(particulasBloque,transform.position,Quaternion.identity);
		adminPuntos.sumarPuntos();
	}

}
