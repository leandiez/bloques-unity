﻿using UnityEngine;
public class Bloques : MonoBehaviour
{
    [SerializeField] Color oneHitLeft, twoHitLeft, threeOrMoreHitLeft;
    [SerializeField] int blockHP = 3;
    public GameObject particulasBloque;
    //public GestorPuntos adminPuntos;

    private void Start()
    {
        ChangeColour();
    }

    void ChangeColour()
    {
        var blockRenderer = GetComponent<Renderer>();
        switch (blockHP)
        {
            case >= 3:
                blockRenderer.material.color = threeOrMoreHitLeft;
                break;
            case 2:
                blockRenderer.material.color = twoHitLeft;
                break;
            case 1:
                blockRenderer.material.color = oneHitLeft;
                break;
            default:
                Destroy(gameObject);
                break;
        }

    }
    void OnCollisionEnter(Collision other)
    {
        blockHP--;
        if (blockHP <= 0)
        {
            Destroy(gameObject);
            transform.SetParent(null);
            Instantiate(particulasBloque, transform.position, Quaternion.identity);
            GameEvents.instance.BlockDestroy();
            //adminPuntos.sumarPuntos();
        }
        else
        {
            ChangeColour();
        }

    }

}
