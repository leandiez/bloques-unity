﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GestorPuntos : MonoBehaviour
{
    public Text textoPuntos;
    public static int puntos = 0;
    public GameObject nivelCompleto;
    public GameObject juegoCompleto;
    public MovPelota pelota;
    public MovBarra barra;
    public CambiarNivel alSiguienteNivel;
    public Transform grupoBloques;
    public SonidosEventos parlante;
    // Start is called before the first frame update
    void Start(){
        ModificarMarcadorPuntos();
    }

    void ModificarMarcadorPuntos(){
        textoPuntos.text = "Puntos: "+puntos;
    }

    public void sumarPuntos(){
        if(grupoBloques.childCount <= 0){
            parlante.ReproducirVictoria();
            pelota.DetenerMovimiento();
            barra.enabled = false;
            if(alSiguienteNivel.EsUltimoNivel()){
                juegoCompleto.SetActive(true);
            }else{
                nivelCompleto.SetActive(true);
            }
            alSiguienteNivel.ActivarCarga();
        }
        puntos++;
        ModificarMarcadorPuntos();
    }
}
