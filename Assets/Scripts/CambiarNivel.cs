﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CambiarNivel : MonoBehaviour
{
    public string nombreNivel;
    public float tiempoRetardo;
    public void ActivarCarga(){
        Invoke("CargarNivel",tiempoRetardo);
    }

    void CargarNivel(){
        SceneManager.LoadScene(nombreNivel);
    }

    public bool EsUltimoNivel(){
        return nombreNivel == "Portada";
    }
}
