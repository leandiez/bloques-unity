﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GestorVidas : MonoBehaviour
{
    public Text textoVidas;
    public static int vidas = 3;
    public MovPelota pelota;
    public MovBarra barra;
    public GameObject gameOver;
    public CambiarNivel volverPortada;
    public SonidosEventos parlante;

    // Start is called before the first frame update
    void Start()
    {
        modificarMarcador();
    }
    void modificarMarcador(){
        textoVidas.text = "Vidas: "+vidas;
    }
    public void restarVida(){
        if(vidas <= 0) return;
        vidas--;
        modificarMarcador();
        if(vidas == 0){
            parlante.ReproducirGameOver();
            gameOver.SetActive(true);
            pelota.DetenerMovimiento();
            //A la barra la desactivo inhabilitando el script de la misma. Quitendole la funcionalidad de moverse. Recorda que MovBarra es un componente del objeto Barra
            barra.enabled = false;
            volverPortada.nombreNivel = "Portada";
            volverPortada.ActivarCarga();
        }else{
            barra.Reset();
            pelota.Reset();
        }

        
    }
}
