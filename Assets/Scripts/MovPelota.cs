﻿using System.Collections;
using UnityEngine;

public class MovPelota : MonoBehaviour
{
    Rigidbody rig;
    public bool enJuego = false;
    public float velocidad;
    Vector3 posicionInicial;
    Vector3 tamañoInicial;
    Quaternion rotacionInicial;
    Transform barraPadre;
    void Awake()
    {
        rig = GetComponent<Rigidbody>();
    }
    // Use this for initialization
    void Start()
    {
        posicionInicial = transform.position;
        tamañoInicial = transform.localScale;
        rotacionInicial = transform.rotation;
        barraPadre = transform.parent;
    }
    /// <summary>
    /// Reset is called when the user hits the Reset button in the Inspector's
    /// context menu or when adding the component the first time.
    /// </summary>
    public void Reset()
    {
        DetenerMovimiento();
        transform.position = posicionInicial;
        transform.rotation = rotacionInicial;
        transform.SetParent(barraPadre);
        transform.localScale = tamañoInicial;
        enJuego = false;
    }

    public void DetenerMovimiento()
    {
        rig.velocity = Vector3.zero;
        rig.isKinematic = true;
    }

    public void DispararPelota()
    {
        transform.parent = null;
        rig.isKinematic = false;
        enJuego = true;
        rig.AddForce(new Vector3(velocidad, velocidad, 0));

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1") && !enJuego)
        {
            DispararPelota();
        }

    }
}
