using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoMoveBar : MonoBehaviour
{
    [SerializeField] GameObject ballObject;
    Vector3 posicionInicial;
    void Start()
    {
        posicionInicial = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(Mathf.Clamp(ballObject.transform.position.x, -8, 8), transform.position.y, transform.position.z);
    }
}
