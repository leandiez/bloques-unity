﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonidosEventos : MonoBehaviour
{
    public AudioSource parlante;
    public AudioClip gameOver,nivelTerminado;

    public void ReproducirGameOver(){
        ReproducirUnaVez(gameOver);
    }

    public void ReproducirVictoria(){
        ReproducirUnaVez(nivelTerminado);
    }

    void ReproducirUnaVez(AudioClip sonido){
        parlante.clip = sonido;
        parlante.playOnAwake = false;
        parlante.loop = false;
        parlante.Play();
    }
}
