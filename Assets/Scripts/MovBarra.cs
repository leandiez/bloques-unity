﻿using UnityEngine;
using System.Collections;

public class MovBarra : MonoBehaviour {

	public float velocidad;
	Vector3 posicionInicial;
	public ElementoInteractivo botonIzquierdo;
	public ElementoInteractivo botonDerecho;

	// Use this for initialization
	void Start () {
		posicionInicial = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		float direccion;
		if(botonIzquierdo.pulsado){
			direccion = -1;
		}else if (botonDerecho.pulsado){
			direccion = 1;
		}else{
			direccion = Input.GetAxisRaw("Horizontal");
		}

		float nuevaPosicionX = transform.position.x+(direccion*velocidad*Time.deltaTime);
		transform.position = new Vector3(Mathf.Clamp(nuevaPosicionX,-8,8),transform.position.y,transform.position.z);
		
	}

	public void Reset(){
		transform.position = posicionInicial;
	}
}
