using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoPlayManager : MonoBehaviour
{
    [SerializeField] MovPelota pelotaScript;
    void Start()
    {
        pelotaScript.DispararPelota();
    }

    void Update()
    {
        if (GameObject.FindGameObjectsWithTag("Bloque").Length == 0)
        {
            pelotaScript.Reset();
        }
    }
}
